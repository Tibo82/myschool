import React, { useState, useContext } from "react";
import { AppContext } from "../../helpers/context";
import { Types } from "../../helpers/reducers";
import Picker from "./picker";

import { add2homeworkDb, readHomeworkDB } from "../../helpers/firestore";

const HomeWork = () => {
  const [textContent, setTextContent] = useState("");
  const [myDate, setMyDate] = useState("");
  const [category, setCategory] = useState("Grammaire");
  const { state, dispatch } = useContext(AppContext);

  const newHomeWork = () => {
    dispatch({
      type: Types.AddHomeWork,
      payload: {
        date: myDate,
        content: [{ category: category, id: "1", topic: textContent }],
      },
    });

    setTextContent("");
    console.log(category, textContent);

    add2homeworkDb({ category: category, textContent: textContent });
    readHomeworkDB();
  };
  return (
    <div className="HomeWork">
      <div className="Description">
        <select
          defaultValue=" "
          name="selectedCategory"
          onChange={(e) => setCategory(e.target.value)}
        >
          <option value="Grammaire">Grammaire</option>
          <option value="Orthographe">Orthographe</option>
          <option value="Dictée">Dictée</option>
          <option value="Histoire">Histoire</option>
        </select>
        <textarea
          placeholder="A faire, instructions, recomandations pour mes élèves..."
          value={textContent}
          onChange={(e) => setTextContent(e.target.value)}
        />

        <button type="submit" onClick={newHomeWork}>
          ADD
        </button>
      </div>
      <Picker onChange={(e: string) => setMyDate(e)} props="" />
    </div>
  );
};

export default HomeWork;
