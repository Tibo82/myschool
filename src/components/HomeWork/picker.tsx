import React, { useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";

type PickerProps = {
  props: string;
  onChange: any;
};
const Picker: React.FC<PickerProps> = (props): JSX.Element => {
  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  const [value, onChange] = useState(new Date());
  return (
    <div>
      <Calendar
        className="react-calendar"
        value={value}
        onClickDay={(day) =>
          props.onChange(day.toLocaleDateString("fr", options))
        }
      />
    </div>
  );
};

export default Picker;
