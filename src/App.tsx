import React from "react";
import HomeWork from "./components/HomeWork";
import { AppProvider } from "./helpers/context";
const App = () => {
  return (
    <div className="App">
      <AppProvider>
        <HomeWork />
      </AppProvider>
    </div>
  );
};

export default App;
