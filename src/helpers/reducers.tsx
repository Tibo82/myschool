import uniqid from "uniqid";

type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? { type: Key }
    : { type: Key; payload: M[Key] };
};

export enum Types {
  AddHomeWork = "ADD_HOMEWORK",
  DeleteHomeWork = "DELETE_HOMEWORK",
  DeleteFullDay = "DELETE_FULL_DAY",
}

type contentType = {
  id: string;
  category: string;
  topic: string;
};

type ItemType = {
  date: string;
  content: contentType[];
};

type ItemPayload = {
  [Types.AddHomeWork]: {
    date: string;
    content: [{ id: string; topic: string; category: string }];
  };
  [Types.DeleteHomeWork]: {
    id: string;
  };
  [Types.DeleteFullDay]: {
    date: string;
  };
};

export type ItemActions = ActionMap<ItemPayload>[keyof ActionMap<ItemPayload>];

export const itemReducer = (state: ItemType[], action: ItemActions) => {
  switch (action.type) {
    case Types.AddHomeWork:
      {
        let matchDate = false;
        let matchCategory = false;
        state.map((item) => {
          if (item.date === action.payload.date) {
            matchDate = true;
            item.content.map((itemContent) => {
              if (itemContent.category === action.payload.content[0].category) {
                matchCategory = true;
                return (itemContent.topic = `${itemContent.topic}\n ${action.payload.content[0].topic}`);
              }
            });
            if (!matchCategory) {
              return (item.content = [
                ...item.content,
                action.payload.content[0],
              ]);
            }
          }
        });
        if (!matchDate) {
          return [
            ...state,
            {
              date: action.payload.date,
              content: action.payload.content,
            },
          ];
        }
      }

      return state;
  }
};
