import React, { createContext, useReducer, Dispatch } from "react";

import { itemReducer, ItemActions } from "./reducers";

type contentType = {
  id: string;
  category: string;
  topic: string;
};
type ItemType = {
  date: string;
  content: contentType[];
};

type InitialStateType = {
  items: ItemType[];
};

export const initialState = {
  items: [
    {
      date: "",
      content: [
        {
          id: "",
          category: "",
          topic: "",
        },
      ],
    },
  ] as ItemType[],
};

const AppContext = createContext<{
  state: InitialStateType;
  dispatch: Dispatch<ItemActions>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = ({ items }: InitialStateType, action: ItemActions) => ({
  items: itemReducer(items, action as ItemActions),
});

const AppProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(mainReducer, initialState);
  return (
    <AppContext.Provider value={{ state, dispatch }}>
      {children}
    </AppContext.Provider>
  );
};

export { AppContext, AppProvider };
