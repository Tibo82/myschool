// import firebase from 'firebase/app'
import Firebase from 'firebase/app';

import 'firebase/firestore';
import 'firebase/auth';

// Get a Firestore instance
const firebase = Firebase
  .initializeApp({
    apiKey: "AIzaSyAdqzetuNcnr_b39dH4Fc893X1ScctjqcY",
    authDomain: "myschool-fe94d.firebaseapp.com",
    projectId: "myschool-fe94d",
    storageBucket: "myschool-fe94d.appspot.com",
    messagingSenderId: "579742156740",
    appId: "1:579742156740:web:68499bea4e534d72acc691",
    measurementId: "G-YL8GBH86EY"
  });
  // .firestore()

// utils
const db = firebase.firestore();
export const auth = firebase.auth();

// collection references
export const homeworkDb = db.collection('homework');
export const add2homeworkDb = (data) => homeworkDb
  .add({ category: data.category, textContent: data.textContent })
  .then(function (docRef) {
    console.log("Document written with ID: ", docRef.id);
  })
  .catch(function (error) {
    console.error("Error adding document: ", error);
  });

export const readHomeworkDB = () => homeworkDb.get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${JSON.stringify(doc.data())}`);
    });
});